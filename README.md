# myquitgtk
[![Build status](https://gitlab.com/juliendehos/myquitgtk/badges/master/build.svg)](https://gitlab.com/juliendehos/myquitgtk/pipelines) 

A simple program for [testing the Nix packaging system](https://juliendehos.gitlab.io/posts/pfw/post27-tp-devops.html#empaqueter-un-projet-distant-devopsmyquitgtk).

