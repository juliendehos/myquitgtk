#include <gtkmm.h>
int main(int argc, char ** argv) {
  Gtk::Main kit(argc, argv);
  Gtk::Window window;
  Gtk::Button button(" Quit v0.2");
  button.signal_clicked().connect(sigc::ptr_fun(&Gtk::Main::quit));
  window.add(button);
  window.show_all();
  kit.run(window);
  return 0;
}
